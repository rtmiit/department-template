---
layout: post
title: Instructions
---

# Installing Jekyll on a Debian based system
## Preparation:
### Basic packages needed:
```
sudo apt update
sudo apt install git
sudo apt-get install ruby-full build-essential zlib1g-dev
```

### Configure bashrc to define new environment variables:
```
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

### Install Jekyll as well as bundler:
```
gem install jekyll bundler
```
Check Jekyll's installed version by typing:
```
jekyll --version
```

### Obtain the latest template from git:
```
git clone https://gitlab.com/rtmiit/department-template.git
```

### Build the template:
Change to the new git directory and do:
```
bundle install
bundle exec jekyll serve
```

Copy the URL generated and paste it in your browser to view your website.


==========================================

## Creating website

### <span style="text-decoration:underline">Title of Website</span>
open **_config.yml** and **_config-deply.yml** files
Replace "Department Name" with the actual name. i.e. Computer Sciece & Engineering and etc.

### <span style="text-decoration:underline">Left Menu</span>
Open **nav.html** inside folder "_includes"
1. Each menu item is called as collection.
2. Menu item is two types.
    1. Direct link to a page (E.g: Join as Faculty and Contact Us)
    2. Drop Down menu (E.g: Academics, Admissions and etc)
3. To add as a Direct Link Menu item:
    1. ```<li><a href="{{ 'facultyJoin.html' | absolute_url }}" class="single"><i class="fa fa-user-plus"></i>Join As Faculty</a></li>```
    2. Replace ```facultyJoin.html``` in the above code with the page with the page you created and change ```Join As Faculty``` to the title of the page.
    3. Even if the file name is ```facultyJoin.md```, you need to type as ```facultyJoin.html``` only.
    4. In the front matter specify the order of the menu item as ```order: 10``` (This is given in Contact-Us page)
4. To add as Drop Down Menu item
    1. copy the below code
        <li>
        <button class="
            dropdown-btn
            {% if page.collection == 'custom' %}
            dropdownactive
            {% endif %}">
            <i class="fa fa-book"></i>Custom Menu
        </button>
        <div class="dropdown-tabs"
            {% if page.collection == 'custom' %}
            style="display:block"
            {% endif %}>
            {% for subsection in site.custom %}
            {% if subsection.display == true %}
            <a href="{{ subsection.url | absolute_url }}">{{ subsection.title }}</a>
            {% endif %}
            {% endfor %}
        </div>
        </li>
    2. Please look in the source of file for ```code block``` of above graphical button named custom.
    3. Replace word "**custom**" with the name of the menu item.
    4. Create a folder starting with underscore (Eg: _custom) in the folder "**sections**"
    5. Start creating pages inside that folder, which will automatically will become Sub Menu items.
    6. Work on "custom" menu and it's pages for much understanding.


### <span style="text-decoration:underline">Slides</span>

1. For faster loading of website we are limited to three slides with lower file size.
2. To replace these slides.
    1. Save your images for slider in the location ``/assets/images/gallery/``
    2. Open file **intro.html** located in ```/sections/_homepage_includes/```
    3. Replace the names of the images such as  slide_1.jpg, slide_2.jpg and slide_3.jpg with the names of your images.
    4. For better SEO Optimization (i.e. Google Search results) replace ``Alt`` with the small description of image (i.e. replace First slide with a three word information of picture).

### <span style="text-decoration:underline">News, Talks and Quick Links</span>

### **News, Awards and Honours**

1. This list will automatically displays the latest information based on date which are saved in the location ``/sections/_news/``
2. Create a post with the Markdown Post Format (i.e. 2020-03-14-title-of-the-news.md) in the above mentioned location.
3. It will be automatically shown on Homepage.
4. This post's front matter should be like below  
``    ---``  
``    title: Title of the post``  
``    layout: post``  
``    ---``
5. For good visibility, number of News items are limited to 5.
6. At the end a Hyper Link "See All" will takes to another page which lists all the news.

### **Talks**

1. This list will automatically displays the latest information based on date which are saved in the location ``/sections/_talks/``
2. Create a post with the Markdown Post Format (i.e. 2020-03-14-title-of-the-talk.md) in the above mentioned location.
3. It will be automatically shown on Homepage.
4. This post's front matter should be like below  
``    ---``  
``    title: Title of the post``  
``    layout: post``  
``    ---``
5. For good visibility, number of Talks items are limited to 5.
6. At the end a Hyper Link "See All" will takes to another page which lists all the Talks.
7. Format of the Talk page is of your interest. We are following a format to give a good understanding of Talk details.

### **Quick Links**

1. This list will automatically displays from the file saved in the location ``/_data/quick-links.yml``
2. A few links already in the file for good understanding.
3. When any external link is to be placed under Quick Links, add extra item named ``direct: true``
4. For good visibility, number of Talks items are limited to 5.

### **Curriculum**

1. Open file curriculum.md located in ``/sections/_academics/``
2. Files regarding curriculum are displayed here.
3. These files are stored in ``/assets/files/filename``
4. Using Markdown format, links are updated in this page.


### **Timetable**

1. Timetable is maintained as a Google sheet and it is embedded in the file **timeTale.md** located in ``/sections/_academics/``


### **Admissions**

1. Admissions related pages are created and stored in the location ``/sectionns/_admissions/``
2. Edit and create new pages by copying the existing content and modify the dummy content with the actual information of program.


### **People**

1. Information regarding Faculty is stored in the file faculty.yml in the YAML format located in ``/_data/``
2. Data will be populated by the information available in the above file.
3. Information regarding Staff is stored in the file staff.yml in the YAML format located in ``/_data/``
4. Data will be populated by the information available in the above file.


### **Custom Menu**

1. Custom menu is given for the reference to create new menu items.
2. Practising with this menu will give better training for creating new menu items.

### **Join as Faculty** and **Contact Us** pages are single pages which are written in Markdown format.